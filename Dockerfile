FROM msh100/rtcw

COPY --chown=game:game defaultcomp.cfg /home/game/rtcwpro/configs/defaultcomp.cfg

RUN chmod +x /home/game/wolfded.x86

EXPOSE 27960/udp

ENTRYPOINT ["/home/game/start"]
